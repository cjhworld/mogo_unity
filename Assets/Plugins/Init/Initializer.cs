﻿using UnityEngine;

public class Initializer : MonoBehaviour
{
	void Start () 
    {
		Mogo.Util.LoggerHelper.Debug ("Initializer");
        gameObject.AddComponent<Driver>();
        DestroyImmediate(this); 
	}
}
